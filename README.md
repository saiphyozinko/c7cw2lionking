*** About the Project ***

This project is based on the HTML, CSS, Javascript, Bootstrap and Python languages. 
The json databased is used in this project. You can view at - https://tw.igs.farm/lionking/. 
You can view the example of full project of the system at - https://www.atgtickets.com/shows/disneys-the-lion-king/lyceum-theatre/calendar/2023-08-29.
Months are generated dynamically based on the actual month of the year. Moreover, 3D calendar shape are used in the project. 
Futhermore, Date and time of the show are shown between the calendar Div. The previous show will disappear from the calendar div.  
If the user click one of the show on the calendar div, the page will redirect to the seat layout.In the searlayout dynamic seat are generated based on the Json data coordinates and user can book the seats by selecting them. 
If the seats are taken by other, the book function will not work for that seat. 
	
*** How To Run? ***

Download the repo, open with the text editor such as virtual studieo code and then run the server.py. Open the http://127.0.0.1:5000 on the browser and enjoy !!

*** Warning ***

Due to the JSON dynamic database 502 bad gateway error, this project will not be working properly. 

*** Copyright ***

This project is only authorized to the Sai Phyo Zin Ko, Edinburgh Napier University (UK). 



**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).